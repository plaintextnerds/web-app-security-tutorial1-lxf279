from django.db import connection
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic

from .models import Question, Choice


class IndexView(generic.ListView):
    template_name = "index.html"
    context_object_name = "latest_question_list"

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by("-pub_date")[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = "polls/detail.html"


def result(request, pk):
    question = get_object_or_404(Question, pk=pk)
    return render(request, "polls/results.html", {"question": question})


def vote(request, question_id):
    result = Choice.objects.raw(
        f"SELECT id, question_id "
        f'FROM "bad_sql_practices_choice" '
        f'WHERE id={request.POST["choice"]}'
    )
    if len(result) == 0:
        q = get_object_or_404(Question, pk=question_id)
        return render(
            request,
            "polls/detail.html",
            {
                "question": q,
                "error_message": "You didn't provide a valid choice.",
            },
        )
    choice_question_id = result[0].question_id
    if choice_question_id != question_id:
        q = get_object_or_404(Question, pk=question_id)
        return render(
            request,
            "polls/detail.html",
            {
                "question": q,
                "error_message": "You didn't select a choice.",
            },
        )
    cursor = connection.cursor()
    cursor.execute(
        f'UPDATE "bad_sql_practices_choice" '
        f"SET votes = votes + 1 "
        f"WHERE question_id = {question_id} "
        f'AND id = {request.POST["choice"]}'
    )
    cursor.execute(
        f'INSERT INTO "bad_sql_practices_vote" '
        f"('email', 'question_id')"
        f'VALUES ({request.POST["email"]}, {question_id})'
    )
    return HttpResponseRedirect(reverse("bad_sql:results", args=(question_id,)))
